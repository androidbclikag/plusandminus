package com.example.increasedecrease

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    var number = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    //    private fun init() {
//        minusButton.setOnClickListener { decrease() }
//        plusButton.setOnClickListener { increase() }
//    }
//    private fun init() {
//        minusButton.setOnLongClickListener { decrease(); true }
//        plusButton.setOnLongClickListener { increase(); true }
//    }
    private fun init() {
        minusButton.setOnClickListener(this)
        plusButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        when (v?.id) {
            (R.id.minusButton) -> decrease()
            (R.id.plusButton) -> increase()
        }
    }


    private fun increase() {

        if (number == 10)
            return
        number += 1
        zeroTextView.text = number.toString()
    }

    private fun decrease() {

        if (number == 1 || number == 0)
            return
        number -= 1
        zeroTextView.text = number.toString()
    }


}
